package com.trilda.primeworkers

import android.content.Context
import android.util.Log
import androidx.work.*
import java.util.*


//val compressionWork = OneTimeWorkRequest.Builder(CompressWorker::class.java)
//val data = Data.Builder()
////Add parameter in Data class. just like bundle. You can also add Boolean and Number in parameter.
//data.putLong("lowBound", 0)
//data.putLong("highBound", 1000)
////Set Input Data
//compressionWork.setInputData(data.build())
////enque worker
//WorkManager.getInstance().enqueue(compressionWork.build())
const val KEY_LOW_NUMBER = "lowNumber"
const val KEY_HIGH_NUMBER = "highNumber"

class PrimeWorker(context: Context, workerParams: WorkerParameters) :
    Worker(context, workerParams) {

    override fun doWork(): Result {

        val low =  inputData.getLong("lowBound",0)
        val high =  inputData.getLong("highBound",1000)

        Log.i("PrimeWorker:", "Starting calculating prime numbers... from $low up to $high")

        var numbers: MutableList<Long> = calculatePrimeNumbers(low, high)

        val sleepDuration = 1000 + Random().nextInt(3000)
        Log.i("PrimeWorker:", "sleepDuration $sleepDuration!")
        Thread.sleep(sleepDuration.toLong())

        Log.i("PrimeWorker:", "Work finished!")

        Log.i("PrimeWorker:", "PrimeNubers:  $numbers")

        val output = Data.Builder()
            .putLongArray("numbersArray", numbers.toLongArray())
            .build()

        // an alternative
        MainActivity.primeNumbersList.addAll(numbers)
        Log.i("PrimeWorker:", "PrimeNubers:  $MainActivity.primeNumbersList")


        return Result.success(output)
    }
}

