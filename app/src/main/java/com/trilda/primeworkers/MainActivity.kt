package com.trilda.primeworkers

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.lifecycle.Observer
import androidx.work.*
import com.trilda.primeworkers.databinding.ActivityMainBinding

const val PRIME_WORKERS_TAG = "PrimeWorkers"

inline fun<reified T : Worker > buildWork() : OneTimeWorkRequest {
    return OneTimeWorkRequestBuilder<T>()
        .build()
}

fun buildPrimeWorker (low: Long, high : Long) : OneTimeWorkRequest {
    val constraints = Constraints.Builder()
        .setRequiresCharging(true)
        .build()

    return OneTimeWorkRequestBuilder<PrimeWorker>()
        .addTag(PRIME_WORKERS_TAG)
        .setInputData( workDataOf(
                KEY_LOW_NUMBER to low,
                KEY_HIGH_NUMBER to high
        )) .setConstraints(constraints)
        .build()
    )
}




class MainActivity : AppCompatActivity() {


    lateinit var binding : ActivityMainBinding

    companion object {
        // NOTE
        // ideally prime numbers should be sotred in a database
        // certainly never ever in this way this is only to test WorkManager and Workers

          var primeNumbersList : MutableList<Long> = mutableListOf<Long>()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val  workManager = WorkManager.getInstance(this)
        workManager.cancelAllWork()


        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.cancel.setOnClickListener {
            workManager.cancelAllWork()
            workManager.cancelAllWorkByTag(PRIME_WORKERS_TAG)
        }

        binding.start.setOnClickListener {

            if (primeNumbersList.isNotEmpty()) {
                primeNumbersList.clear()
            }

            val primeOneWorker = buildPrimeWorker(1,800)

            workManager
                .getWorkInfoByIdLiveData(primeOneWorker.id)
                .observe(this, Observer { it ->
                    Log.i("MainActivity:", "PrimeOneWorker workStatus= $it")
                    if (it.state == WorkInfo.State.SUCCEEDED) {
                        Log.i("MainActivity:", "PrimeOneWorker SUCCEDED")

                    }
                })

            val primeTwoWorker = buildPrimeWorker(1000,5000)

            workManager
                .getWorkInfoByIdLiveData(primeTwoWorker.id)
                .observe(this, Observer { it ->
                    Log.i("MainActivity:", "PrimeTwoWorker workStatus= $it")
                    if (it.state == WorkInfo.State.SUCCEEDED) {
                        Log.i("MainActivity:", "PrimeTwoWorker SUCCEDED")
                    }
                })

            val constraints = Constraints.Builder()
                .setRequiredNetworkType(NetworkType.CONNECTED)
                .build()

            val uploadWorker = OneTimeWorkRequest.Builder(UploadWorker::class.java)
                .addTag(PRIME_WORKERS_TAG)
                .setInputMerger(ArrayCreatingInputMerger::class.java)
                .setConstraints(constraints)
                .build()

            workManager
                .getWorkInfoByIdLiveData(uploadWorker.id)
                .observe(this, Observer { it ->
                    Log.i("MainActivity:", "uploadWorker workStatus= $it")
                    if (it.state == WorkInfo.State.SUCCEEDED) {
                        Log.i("MainActivity:", "uploadWorker SUCCEEDED")
                    }
                })

            workManager
                .beginWith(listOf(primeOneWorker,primeTwoWorker))
                .then(uploadWorker)
                .enqueue()
        }
    }
}